<?php

$loader = include('vendor/autoload.php');
$loader->add('', 'src');

$app = new Silex\Application;
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// Fait remonter les erreurs
$app['debug'] = true;

$app['model'] = new Cinema\Model(
    'localhost',  // Hôte
    'cinema',    // Base de données
    'root',    // Utilisateur
    ''     // Mot de passe
);

// Page d'accueil
$app->match('/', function() use ($app) {
    return $app['twig']->render('home.html.twig');
})->bind('home');

// Liste des films
$app->match('/films', function() use ($app) {
    return $app['twig']->render('films.html.twig', array(
        'films' => $app['model']->getFilms()
    ));
})->bind('films');

// Fiche film
$app->match('/film/{id}', function($id) use ($app) {
    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('nom') && $post->has('note') && $post->has('critique')) {
            if(is_numeric($_POST['note']) && $_POST['note'] >= 0 && $_POST['note'] < 6 ){
                return $app['twig']->render('film.html.twig', array(
                'film' => $app['model']->ajoutCritique($_POST["nom"],$_POST["critique"],$_POST["note"],$id),
				'film' => $app['model']->getFilm($id),
                'casting' => $app['model']->getCasting($id),
                'critiques' => $app['model']->getCritiques($id),
				));
            }
        }
    }

    return $app['twig']->render('film.html.twig', array(
        'film' => $app['model']->getFilm($id),
        'casting' => $app['model']->getCasting($id),		      
        'critiques' => $app['model']->getCritiques($id),
    ));
})->bind('film');

// Meilleurs films
$app->match('/meilleursFilms', function() use ($app) {
    return $app['twig']->render('meilleursFilms.html.twig', array(
        'meilleursFilms' => $app['model']->getMeilleursFilms()
    ));
})->bind('meilleursFilms');

// Genres
$app->match('/genres', function() use ($app) {
    return $app['twig']->render('genres.html.twig', array(
        'genres' => $app['model']->getGenres()
    ));
})->bind('genres');

//Films par genres
$app->match('/filmParGenre{id}', function($id) use ($app) {
    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
    }
    return $app['twig']->render('TousfilmParGenre.html.twig', array(
        'filmParGenre' => $app['model']->getTousFilmsParGenre($id)
    ));
})->bind('filmParGenre');

// Ajout de films
$app->match('/ajoutDeFilm', function() use ($app) {
     $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('annee') && $post->has('description') && $post->has('nom') && $post->has('genre') && $post->has('image')) {
            if(is_numeric($_POST['annee']) && $_POST['annee'] >=0 && $_POST['annee'] <2017 ){
                return $app['twig']->render('home.html.twig', array(
                'film' => $app['model']->ajoutFilm($_POST["annee"],$_POST["description"],$_POST["nom"],$_POST["genre"],$_POST["image"]),
				));
            }          
        }
    }
    return $app['twig']->render('ajoutDeFilm.html.twig', array(
        'genres' => $app['model']->getGenres()
    ));
})->bind('ajoutDeFilm');
$app->run();
