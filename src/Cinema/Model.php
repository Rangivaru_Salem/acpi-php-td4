<?php

namespace Cinema;

/**
 * Représente le "Model", c'est à dire l'accès à la base de
 * données pour l'application cinéma basé sur MySQL
 */
class Model
{
    protected $pdo;

    public function __construct($host, $database, $user, $password)
    {
        try {
            $this->pdo = new \PDO(
                'mysql:dbname='.$database.';host='.$host,
                $user,
                $password
            );
        } catch (\PDOException $error) {
            die('Unable to connect to database.');
        }
        $this->pdo->exec('SET CHARSET UTF8');
    }

    protected function execute(\PDOStatement $query, array $variables = array())
    {
        if (!$query->execute($variables)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Récupère un résultat exactement
     */
    protected function fetchOne(\PDOStatement $query)
    {
        if ($query->rowCount() != 1) {
            return false;
        } else {
            return $query->fetch();
        }
    }

    /**
     * Base de la requête pour obtenir un film
     */
    protected function getFilmSQL()
    {
        return
            'SELECT films.image, films.id, films.nom, films.description, genres.nom as genre_nom FROM films 
             INNER JOIN genres ON genres.id = films.genre_id ';
    }

    /**
     * Récupère la liste des films
     */
    public function getFilms()
    {
        $sql = $this->getFilmSQL();

        return $this->execute($this->pdo->prepare($sql));
    }

    /**
     * Récupère un film
     */
    public function getFilm($id)
    {
        $sql = 
            $this->getFilmSQL() . 
            'WHERE films.id = ?'
            ;

        $query = $this->pdo->prepare($sql);
        $this->execute($query, array($id));

        return $this->fetchOne($query);
    }

    /**
     * Récupérer les acteurs d'un film
     */
    public function getCasting($filmId)
    {
        $listeActeur = array();
        $sql = "SELECT a.prenom, a.nom, a.image, r.role FROM acteurs a, roles r, films f
		WHERE a.id = r.acteur_id AND r.film_id = f.id
        AND f.id = ?";
        $query = $this->pdo->prepare($sql);
        return $this->execute($query,array($filmId));
    }

    /**
     * Genres
     */
    public function getGenres()
    {
        $sql = 'SELECT g.nom, g.id, COUNT(*) as nb_films FROM genres g,films f
            WHERE f.genre_id = g.id 
            GROUP BY g.id';
        return $this->execute($this->pdo->prepare($sql));
    }
	
    //retourne les critiques d'un film
    public function getCritiques($filmId)
    {
        $sql = "SELECT nom,commentaire,note FROM critiques  
		WHERE film_id = ?";
        $query = $this->pdo->prepare($sql);
        return  $this->execute($query,array($filmId));    
    }
	//retourne les meilleur film
	public function getMeilleursFilms()
    {				
        $sql = "SELECT f.id, f.nom, f.description, f.image, g.nom as genres_nom, ROUND(avg(c.note),1) as moyenne 
				FROM films f , critiques c, genres g
				WHERE f.id = c.film_id AND f.genre_id = g.id
                GROUP BY f.nom
				ORDER BY moyenne DESC";
		$query = $this->pdo->prepare($sql);
        return $this->execute($this->pdo->prepare($sql));
	}
	// retourne tous les films par genre saisie
	public function getTousFilmsParGenre($idGenre)
    {      
        $sql = "SELECT f.image, f.id, f.nom, f.description, g.nom as genre_nom , g.id
		FROM films f, genres g
		WHERE f.genre_id = g.id  AND g.id = ?";
        $query = $this->pdo->prepare($sql);
        return  $this->execute($query,array($idGenre));
	}
	
    //Ajoute une critique
    public function ajoutCritique($nom,$commentaire,$note,$filmId)
    {
      $insert = $this->pdo->prepare('INSERT INTO critiques (nom,commentaire,note,film_id)VALUES 
	  (?,?,?,?)');

        $insert->execute(array($nom, $commentaire,$note,$filmId));
    }
	
    //Ajoute un film
    public function ajoutFilm($annee,$description,$nom,$genre,$image)
    {
      $insert = $this->pdo->prepare('INSERT INTO films (annee,description,nom,genre_id,image) VALUES 
	  (?,?,?,?,?)');
        $insert->execute(array($annee, $description,$nom,$genre,$image));  
    }
}
